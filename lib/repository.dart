import 'package:http/http.dart' as http;
import 'dart:convert';

class Repository {
  final String symbol_set;
  final String symbol;
  final String host = "https://apiv2.bitcoinaverage.com/indices/";

  Repository(this.symbol_set, this.symbol);

  Future<dynamic> getLastCurrency() async {
    StringBuffer stringBuffer = StringBuffer(host);
    stringBuffer.write(symbol_set);
    stringBuffer.write("/ticker/");
    stringBuffer.write(symbol);

    return await _getLastCurrencyFromResponce(stringBuffer.toString());
  }

  Future _getLastCurrencyFromResponce(String request) async {
    http.Response response = await http.get(request);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return jsonDecode(response.body);
    }
    print('connection error:' + response.statusCode.toString());
  }

}