import 'package:flutter/material.dart';

const List<String> currenciesList = [
  'AUD',
  'BRL',
  'CAD',
  'CNY',
  'EUR',
  'GBP',
  'HKD',
  'IDR',
  'ILS',
  'INR',
  'JPY',
  'MXN',
  'NOK',
  'NZD',
  'PLN',
  'RON',
  'RUB',
  'SEK',
  'SGD',
  'USD',
  'ZAR'
];

const List<String> cryptoList = [
  'BTC',
  'ETH',
  'LTC',
];

class CoinData {
  List<DropdownMenuItem> getDropdownButtonsList() {
    List<DropdownMenuItem<String>> outputList = List();
    currenciesList.forEach((currency) {
      outputList.add(
        DropdownMenuItem(
          child: Text(currency),
          value: currency,
        ),
      );
    });
    return outputList;
  }

  List<String> getCurrenciesList() {
    return currenciesList;
  }
}
