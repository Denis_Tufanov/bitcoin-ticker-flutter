import 'package:bitcoin_ticker/coin_data.dart';
import 'package:bitcoin_ticker/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io' show Platform;

class PriceScreen extends StatefulWidget {
  @override
  _PriceScreenState createState() => _PriceScreenState();
}

class _PriceScreenState extends State<PriceScreen> {
  var selectedCurrency = "USD";
  var valueOfCurrency = "?";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('🤑 Coin Ticker'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 0),
            child: Card(
              color: Colors.lightBlueAccent,
              elevation: 5.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 28.0),
                child: Text(
                  '1 BTC = $valueOfCurrency $selectedCurrency',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          Container(
            height: 150.0,
            alignment: Alignment.center,
            padding: EdgeInsets.only(bottom: 30.0),
            color: Colors.lightBlue,
            child: Platform.isIOS
                ? _getiOsDropdownPicker()
                : _getAndroidDropdownPicker(),
          ),
        ],
      ),
    );
  }

  Widget _getiOsDropdownPicker() {
    return CupertinoPicker(
      backgroundColor: Colors.lightBlue,
      itemExtent: 32.0,
      onSelectedItemChanged: (selectedIndex) {
        setState(() {
          selectedCurrency =
              CoinData().getCurrenciesList().elementAt(selectedIndex);
          var lastCurrency =
              Repository("global", "BTC" + selectedCurrency).getLastCurrency();
          valueOfCurrency = getLastCurrency(lastCurrency);
        });
      },
      children: CoinData().getDropdownButtonsList(),
    );
  }

  Widget _getAndroidDropdownPicker() {
    return DropdownButton<String>(
      value: selectedCurrency,
      items: CoinData().getDropdownButtonsList(),
      onChanged: (value) {
        setState(() {
          selectedCurrency = value;
          var lastCurrency =
              Repository("global", "BTC" + selectedCurrency).getLastCurrency();
          valueOfCurrency = getLastCurrency(lastCurrency);
        });
      },
    );
  }

  String getLastCurrency(lastCurrency) => lastCurrency["last"].toString();
}
